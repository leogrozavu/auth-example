const express = require('express')
const app = express()
const mongoose = require('mongoose')
const authRoute = require('./routes/auth')
const cors = require('cors');
const baseRoute = require('./routes/base')
const bodyParser = require('body-parser');
const postRoute = require('./routes/posts')
// app.use(
bodyParser.json();
// app.use(bodyParser.text());
app.use(express.json())
app.use(cors());

mongoose.connect(
    'mongodb+srv://leo:123leo890ciupapi@cluster0.lmysp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    { useNewUrlParser :true, useUnifiedTopology: true},
    () => console.log('connected to db')
)

app.use('/api/user', authRoute)

app.use('/', baseRoute)
app.use('/api/posts', postRoute)

let server = app.listen(3000, () => {
    console.log(`express server listening on ${server.address().port}`);
})
