const router = require('express').Router()
const User = require('../model/user')
const { registerValidation,loginValidation }= require("./validation")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const user = require('../model/user')


router.post('/register', async (req,res) => {
    console.log('endpoint hit');
    const { error } = registerValidation(req.body)
    if (error) return res.status(400).send(error.details[0].message)

    //check if email is used
    const emailExist = await User.findOne({ email: req.body.email })
    if (emailExist) return res.status(400).send("email already exist")

    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt)
   
    //create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    })
    try{
        const savedUser = await user.save()
        return res.send(savedUser)
    }catch(err){
        return res.status(400).send(err)
    }
})


///login

router.post('/login', async (req,res) => {
    const { error } = loginValidation(req.body)
    if (error) return res.status(400).send(error.details[0].message)

//checking if email exist
    const emailExist = await User.findOne({ email: req.body.email })
    if (!emailExist) return res.status(400).send("Email not found")

//password
    console.log(`${req.body.password} = ${emailExist.password}`)
    const validPass = await bcrypt.compare(req.body.password, emailExist.password)
    if(!validPass) return res.status(400).send("Invalid password")

//token
    const token = jwt.sign({ _id: user._id }, `${process.env.secret_token}`)
    res.header('auth-token', token).send(token)
})

module.exports = router